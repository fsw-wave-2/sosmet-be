"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class journal extends Model {
    static associate(model) {}
  }
  journal.init(
    {
      journal_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: "user",
          key: "user_id",
        },
        field: "user_id",
      },
      thumbnail: DataTypes.STRING,
      title: DataTypes.TEXT,
      excerpt: DataTypes.TEXT,
      content: DataTypes.TEXT,
      set_public: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "journal",
    }
  );

  journal.associate = function (models) {
    journal.belongsTo(models.user, {
      foreignKey: "user_id",
    });
  };

  return journal;
};
