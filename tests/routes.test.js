const request = require("supertest");
const app = require("../app");
//
//debug
console.log("app:\n", app.post);

describe("Endpoints Public", () => {
  it("should create a new post", async () => {
    const res = await request(app).post("/api/v1/register").send({
      username: "santest",
      email: "santest@email.com",
      password: "123",
    });
    expect(res.statusCode).toEqual(201);
    expect(res.body);
    //
    //debug
    // console.log(
    //   "\n/api/v1/register\n",
    //   "\nres:\n",
    //   res.header,
    //   "\n",
    //   "\nres.statusCode:\n",
    //   res.statusCode,
    //   "\n",
    //   "\nres.body:\n",
    //   res.body,
    //   "\n"
    // );
  });
  it("fetch users", async () => {
    const res = await request(app).get("/api/v1/all-profiles");
    expect(res.statusCode).toEqual(200);
    expect(res.body);
    //
    //debug
    // console.log(
    //   "\n/api/v1/all-profiles\n",
    //   "\nres:\n",
    //   res.header,
    //   "\n",
    //   "\nres.statusCode:\n",
    //   res.statusCode,
    //   "\n",
    //   "\nres.body:\n",
    //   res.body,
    //   "\n"
    // );
  });
  it("fetch posts", async () => {
    const res = await request(app).get("/api/v1/posts");
    expect(res.statusCode).toEqual(200);
    expect(res.body);
    //
    //debug
    // console.log(
    //   "\n/api/v1/posts\n",
    //   "\nres:\n",
    //   res.header,
    //   "\n",
    //   "\nres.statusCode:\n",
    //   res.statusCode,
    //   "\n",
    //   "\nres.body:\n",
    //   res.body,
    //   "\n"
    // );
  });

  it("fetch public info user detail", async () => {
    const user_id = 1;
    const res = await request(app).get(`/api/v1/public-user/${user_id}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body);
    //
    //debug
    // console.log(
    //   "\napi/v1/public-user/1\n",
    //   "\nres:\n",
    //   res.header,
    //   "\n",
    //   "\nres.statusCode:\n",
    //   res.statusCode,
    //   "\n",
    //   "\nres.body:\n",
    //   res.body,
    //   "\n"
    // );
  });

  it("fetch posts by user id", async () => {
    const res = await request(app).get("/api/v1/posts/1");
    expect(res.statusCode).toEqual(200);
    expect(res.body);
    //
    //debug
    // console.log(
    //   "\napi/v1/posts/1\n",
    //   "\nres:\n",
    //   res.header,
    //   "\n",
    //   "\nres.statusCode:\n",
    //   res.statusCode,
    //   "\n",
    //   "\nres.body:\n",
    //   res.body,
    //   "\n"
    // );
  });

  it("fetch comments", async () => {
    const res = await request(app).get("/api/v1/comments");
    expect(res.statusCode).toEqual(200);
    expect(res.body);
    //
    //debug
    // console.log(
    //   "\napi/v1/comments\n",
    //   "\nres:\n",
    //   res.header,
    //   "\n",
    //   "\nres.statusCode:\n",
    //   res.statusCode,
    //   "\n",
    //   "\nres.body:\n",
    //   res.body,
    //   "\n"
    // );
  });
});
