const Router = require("express-group-router");
const router = new Router();
const controllerUser = require("../controllers/api/v1/UserControllers");
const controllerPost = require("../controllers/api/v1/PostControllers");
const controllerComment = require("../controllers/api/v1/CommentControllers");
const controllerLove = require("../controllers/api/v1/LoveControllers");
const controllerFollow = require("../controllers/api/v1/FollowControllers");
const controllerStorage = require("../controllers/api/v1/StorageControllers");
const controllerJournal = require("../controllers/api/v1/JournalControllers");
// middleware
const middlewares = require("../middlewares/middlewares");
const upload = require("../utils/multer");
const uploadFile = require("../utils/multer-file");
const uploadVideo = require("../utils/multer-video");
const uploadAudio = require("../utils/multer-audio");

module.exports = (app) => {
  /**
   * Auth Route
   */
  // register
  router.post("/register", controllerUser.userRegister);
  // login
  router.post("/login", controllerUser.userLogin);
  
  // Api route public
  // show all users
  router.get("/all-profiles", controllerUser.getAllProfiles);
  router.get("/random-users", controllerUser.getRandomUsers);
  // show detail profile
  router.get("/public-user/:id", controllerUser.getProfile);

  // Post route
  router.get("/posts", controllerPost.index);
  router.get("/post/:id", controllerPost.show);
  router.get("/posts/:id", controllerPost.getPostByUser);

  // Comment route
  router.get("/comments", controllerComment.index);
  router.get("/comment/:id", controllerComment.show);
  router.get("/comments/:id", controllerComment.getCommentByUser);

  // Love route
  router.get("/total-Love/:post_id", controllerLove.totalLove);
  router.get("/love-this-post/:post_id", controllerLove.loveThisPost);

  // Publish Journal
  router.get(
    "/journals-publish-user/:id",
    controllerJournal.getPublishJournalsByUser
  );
  router.get("/journal-public/:id", controllerJournal.getPublicJournal);

  /**
   *   Route with middleware
   */
  router.group([middlewares.verifyToken], (router) => {
    // showing my profile
    router.get("/user/profile/me", controllerUser.getMyProfile);
    // update profile
    router.put("/user/:id", controllerUser.updateProfile);
    router.put(
      "/avatar/:id",
      upload.single("image"),
      controllerUser.uploadProfilePict
    );
    // create new post
    router.post(
      "/new-post-image/:id",
      upload.single("image"),
      controllerPost.createPostImage
    );
    // route delete post
    router.delete("/post-destroy/:id", controllerPost.delete);
    router.post("/new-post-text", controllerPost.createPostText);
    // route new comment
    router.post("/comment-post", controllerComment.create);
    // route new love post
    router.post("/love-post", controllerLove.lovedPost);
    // route following user
    router.post("/following", controllerFollow.following);
    // route unfollowing user
    router.post("/unfollowing", controllerFollow.unfollowing);

    //route storage
    router.post(
      "/upload-doc/:id",
      uploadFile.single("pdf"),
      controllerStorage.uploadDoc
    );

    router.post(
      "/upload-video/:id",
      uploadVideo.single("video"),
      controllerStorage.uploadVideo
    );

    router.post(
      "/upload-audio/:id",
      uploadAudio.single("audio"),
      controllerStorage.uploadAudio
    );

    router.post(
      "/upload-image/:id",
      upload.single("image"),
      controllerStorage.uploadImage
    );

    router.get("/user", controllerUser.getProfileByUsername);
    router.get("/storage-images/:id", controllerStorage.fetchImages);
    router.get("/storage-videos/:id", controllerStorage.fetchVideos);
    router.get("/storage-docs/:id", controllerStorage.fetchDocs);
    router.get("/storage-audios/:id", controllerStorage.fetchAudios);

    router.get("/storage-image-details/:id", controllerStorage.fetchImage);
    router.get("/storage-video-details/:id", controllerStorage.fetchVideo);
    router.get("/storage-doc-details/:id", controllerStorage.fetchDoc);
    router.get("/storage-auido-details/:id", controllerStorage.fetchAudio);

    //route journal
    router.post("/journal-create", controllerJournal.createJournal);
    router.get(
      "/journals-user/:id",
      controllerJournal.getPrivateJournalsByUser
    );
    router.get("/journal/:id", controllerJournal.getPrivateJournal);
    router.put("/journal-update/:id", controllerJournal.updateJournal);
    router.put("/journal-publish/:id", controllerJournal.publishJournal);
  });

  const listRoutes = router.init();
  app.use("/api/v1/", listRoutes);
};
