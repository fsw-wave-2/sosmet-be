const db = require("../../../models");
const Journal = db.journal;
const User = db.user;
const sequelize = db.Sequelize;

module.exports = {
  index: (req, res) => {
    const content = req.query.content;

    var condition = content
      ? { content: { [Op.iLike]: `%${content}%` } }
      : null;

    Journal.findAll({
      where: condition,
      include: [
        {
          model: User,
          attributes: {
            exclude: [
              "birthdate",
              "email",
              "password",
              "gender",
              "address",
              "createdAt",
              "updatedAt",
            ],
          },
        },
      ],
      order: [["createdAt", DESC]],
      limit: 10,
      offset: (1 - 1) * 10,
    })
      .then((data) => {
        res.status(200).send({
          status: "success",
          data,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error ocurred while retrieving Journals",
        });
      });
  },

  getPrivateJournalsByUser: (req, res) => {
    const user_id = req.params.id;
    // const set_public = false;

    Journal.findAll({
      where: { user_id: user_id },
      include: [
        {
          model: User,
          attributes: {
            exclude: [
              "birthdate",
              "email",
              "password",
              "gender",
              "address",
              "createdAt",
              "updatedAt",
            ],
          },
        },
      ],
      order: [["createdAt", "DESC"]],
      // limit: 10,
      // offset: (1 - 1) * 10,
    })
      .then((data) => {
        if (data.length !== 0) {
          res.status(200).send({
            status: "success",
            data,
          });
        } else {
          res.status(201).send({
            status: "success",
            user_id: user_id,
            message: "hasn't written a journal yet",
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error ocurred while retrieving Journals",
        });
      });
  },

  getPublishJournalsByUser: (req, res) => {
    const user_id = req.params.id;
    const set_public = true;

    Journal.findAll({
      where: {
        user_id: user_id,
        set_public: set_public,
      },
      include: [
        {
          model: User,
          attributes: {
            exclude: [
              "user_id",
              "birthdate",
              "email",
              "password",
              "gender",
              "address",
              "createdAt",
              "updatedAt",
            ],
          },
        },
      ],
    })
      .then((data) => {
        if (data.length !== 0) {
          res.status(200).send({
            status: "success",
            data,
          });
        } else {
          res.status(201).send({
            status: "success",
            user_id: user_id,
            message: "hasn't written a journal yet",
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error ocurred while retrieving Journals",
        });
      });
  },

  getPrivateJournal: (req, res) => {
    const journal_id = req.params.id;

    Journal.findByPk(journal_id, {
      include: [
        {
          model: User,
          attributes: {
            exclude: [
              "birthdate",
              "email",
              "password",
              "gender",
              "address",
              "createdAt",
              "updatedAt",
            ],
          },
        },
      ],
    })
      .then((data) => {
        if (!data) {
          return res.status(401).send({
            status: "failed",
            message: "Journal not found",
          });
        }
        res.status(200).send({
          status: "success",
          data,
        });
      })
      .catch((err) => {
        res.status(500).send({
          journal_id: journal_id,
          message: err.message || "error when retrieving your journal",
        });
      });
  },

  getPublicJournal: (req, res) => {
    const id = req.params.id;
    const set_public = true;

    Journal.findByPk(id, {
      where: { set_public: set_public },
      include: [
        {
          model: User,
          attributes: {
            exclude: [
              "birthdate",
              "email",
              "password",
              "gender",
              "address",
              "createdAt",
              "updatedAt",
            ],
          },
        },
      ],
    })
      .then((data) => {
        if (!data) {
          return res.status(401).send({
            status: "failed",
            message: "Journal not found",
          });
        }
        res.status(200).send({
          status: "success",
          data,
        });
      })
      .catch((err) => {
        res.status(500).send({
          journal_id: id,
          message: err.message || "error when retrieving your journal",
        });
      });
  },

  createJournal: (req, res) => {
    const new_journal = {
      user_id: req.body.user_id,
      thumbnail: req.body.thumbnail,
      title: req.body.title,
      excerpt: req.body.excerpt,
      content: req.body.content,
    };

    Journal.create(new_journal)
      .then((data) => {
        res.status(200).send({
          status: "success",
          message: "journals was created",
          journal_id: data.journal_id,
          set_public: data.set_public,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "error create post",
        });
      });
  },

  updateJournal: (req, res) => {
    const { id } = req.params;

    // const { journal_id } = req.body;

    Journal.update(req.body, {
      where: { journal_id: id },
    })
      .then((num) => {
        if (num == 1) {
          res.send({
            journal_id: id,
            message: "journal was updated successfully",
          });
          // console.log(num);
        } else {
          res.send({
            journal_id: id,
            message: "can't update your journal",
          });
          // console.log(num);
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "error update profile",
        });
      });
  },
  publishJournal: (req, res) => {
    const { id } = req.params.id;

    Journal.update(req.body, {
      where: { journal_id: id },
    })
      .then((num) => {
        if (num == 1) {
          res.status(200).send({
            journal_publish: true,
            message: "journals was published",
          });
        } else {
          res.status(201).send({
            journal_publish: false,
            message: "journals failed to publish",
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "occured error when publish journals",
        });
      });
  },
};
