"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class storageaudio extends Model {
    static associate(models) {}
  }

  storageaudio.init(
    {
      storage_audio_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: "users",
          key: "user_id",
        },
        field: "user_id",
      },
      audio_name: DataTypes.STRING,
      audio_link: DataTypes.STRING,
      cloudinary_id: DataTypes.STRING,
    },
    { sequelize, modelName: "storage_audio" }
  );
  storageaudio.associate = function (models) {
    storageaudio.belongsTo(models.user, {
      foreignKey: "user_id",
    });
  };

  return storageaudio;
};
