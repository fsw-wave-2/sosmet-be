const db = require("../../../models");
const StorageFile = db.storagefile;
const StorageImage = db.storageimage;
const StorageVideo = db.storagevideo;
const StorageAudio = db.storageaudio;
const User = db.user;
const cloudinary = require("../../../utils/cloudinary");

module.exports = {
  uploadDoc: async (req, res) => {
    try {
      const { id } = req.params;
      const post_file = await cloudinary.uploader.upload(req.file.path, {
        resource_type: "image",
      });

      //
      //debugging
      // console.log("secure_url:\n" + post_file.secure_url, "\n");
      // console.log("cloudinary_id:\n" + post_file.public_id, "\n\n");

      const new_post_file = {
        user_id: id,
        file_link: post_file.secure_url,
        cloudinary_id: post_file.public_id,
      };

      await StorageFile.create(new_post_file)
        .then(
          res.status(200).send({
            status: "success",
            message: "file successfully uploaded",
          })
        )
        .catch((err) => {
          res.status(500).send({
            message: err.message || "some error occured whole uploaded file",
          });
        });
    } catch (err) {
      // console.log("error_message_upload_file:", err);
      res.status(500).send({
        message: err.message
      });
    }
  },
  uploadImage: async (req, res) => {
    try {
      const { id } = req.params;
      const post_image = await cloudinary.uploader.upload(req.file.path);

      //
      //
      // console.log("cloudinary:\n", cloudinary.uploader.upload(req.file.path));
      //
      //debugging
      // console.log("secure_url:" + post_image.secure_url);
      // console.log("cloudinary_id:" + post_image.public_id);

      const new_post_image = {
        user_id: id,
        image_link: post_image.secure_url,
        cloudinary_id: post_image.public_id,
      };

      await StorageImage.create(new_post_image)
        .then((data) => {
          res.status(200).send({
            status: "success",
            message: "image successfully uploaded",
            image_link: data.image_link,
          });
          // res.status(200).send(data);
        })
        .catch((err) => {
          res.status(500).send({
            message: err.message || "some error occured whole uploaded image",
          });
        });
    } catch (err) {
      // console.log("error_message_upload_image:", err);
      res.status(500).send({
        message: err.message
      });
    }
  },
  uploadVideo: async (req, res) => {
    try {
      const { id } = req.params;
      const post_video = await cloudinary.uploader.upload(req.file.path, {
        resource_type: "video",
      });

      //
      //debugging
      // console.log("secure_url:" + post_video.secure_url);
      // console.log("cloudinary_id:" + post_video.public_id);

      const new_post_video = {
        user_id: id,
        video_link: post_video.secure_url,
        cloudinary_id: post_video.public_id,
      };

      await StorageVideo.create(new_post_video)
        .then(
          res.status(200).send({
            status: "success",
            message: "video successfully uploaded",
          })
        )
        .catch((err) => {
          res.status(500).send({
            message: err.message || "some error occured whole uploaded video",
          });
        });
    } catch (err) {
      // console.log("error_message_upload_video:", err);
      res.status(500).send({
        message: err.message
      });
    }
  },
  uploadAudio: async (req, res) => {
    try {
      const { id } = req.params;
      const post_audio = await cloudinary.uploader.upload(req.file.path, {
        resource_type: "video",
      });

      //
      //debugging
      // console.log("secure_url:" + post_audio.secure_url);
      // console.log("cloudinary_id:" + post_audio.public_id);

      const new_post_audio = {
        user_id: id,
        audio_link: post_audio.secure_url,
        cloudinary_id: post_audio.public_id,
      };

      await StorageAudio.create(new_post_audio)
        .then(
          res.status(200).send({
            status: "success",
            message: "audio successfully uploaded",
          })
        )
        .catch((err) => {
          res.status(500).send({
            message: err.message || "some occured whole upload audio",
          });
        });
    } catch (err) {
      // console.log("error_message_upload_audio:", err);
      res.status(500).send({
        message: err.message
      });
    }
  },
  fetchImages: (req, res) => {
    const { id } = req.params;

    StorageImage.findAll({
      where: { user_id: id },
      order: [["createdAt", "DESC"]],
    })
      .then((data) => {
        if (data.length !== 0) {
          res.status(200).send({
            status: "success",
            data,
          });
        } else {
          res.status(200).send({
            status: "success",
            user_id: id,
            message: "you have no images",
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "some error occured while retrieving images",
        });
      });
  },

  fetchImage: (req, res) => {
    const id = req.params.id;

    StorageImage.findByPk(id, {
      include: [
        {
          model: User,
          attributes: {
            exclude: [
              "password",
              "email",
              "birthdate",
              "gender",
              "address",
              "createdAt",
              "updatedAt",
            ],
          },
        },
      ],
    })
      .then((data) => {
        if (!data) {
          return res.status(401).send({
            status: "failed",
            message: " Storage Image Not Found",
          });
        }
        res.status(200).send({
          status: "success",
          data,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "error when retriving image with id " + id,
        });
      });
  },

  fetchVideos: (req, res) => {
    const { id } = req.params;

    StorageVideo.findAll({
      where: { user_id: id },
      order: [["createdAt", "DESC"]],
    })
      .then((data) => {
        if (data.length !== 0) {
          res.status(200).send({
            status: "success",
            data,
          });
        } else {
          res.status(200).send({
            status: "success",
            user_id: id,
            message: "you have no videos",
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "some error occured while retrieving video",
        });
      });
  },

  fetchVideo: (req, res) => {
    const id = req.params.id;

    StorageVideo.findByPk(id, {
      include: [
        {
          model: User,
          attributes: {
            exclude: [
              "password",
              "email",
              "birthdate",
              "gender",
              "address",
              "createdAt",
              "updatedAt",
            ],
          },
        },
      ],
    })
      .then((data) => {
        if (!data) {
          return res.status(401).send({
            status: "failed",
            message: " Storage Video Not Found",
          });
        }
        res.status(200).send({
          status: "success",
          data,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "error when retriving video with id " + id,
        });
      });
  },

  fetchAudios: (req, res) => {
    const { id } = req.params;

    StorageAudio.findAll({
      where: { user_id: id },
      order: [["createdAt", "DESC"]],
    })
      .then((data) => {
        if (data.length !== 0) {
          res.status(200).send({
            status: "success",
            data,
          });
        } else {
          res.status(200).send({
            status: "success",
            user_id: id,
            message: "You Have No Audios",
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "some error occured while retriving audio",
        });
      });
  },

  fetchAudio: (req, res) => {
    const id = req.params.id;

    StorageAudio.findByPk(id, {
      include: [
        {
          model: User,
          attributes: {
            exclude: [
              "password",
              "email",
              "birthdate",
              "gender",
              "address",
              "createdAt",
              "updatedAt",
            ],
          },
        },
      ],
    })
      .then((data) => {
        if (!data) {
          return res.status(401).send({
            status: "failed",
            message: " Storage Audio Not Found",
          });
        }
        res.status(200).send({
          status: "success",
          data,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "error when retriving audio with id " + id,
        });
      });
  },

  fetchDocs: (req, res) => {
    const { id } = req.params;

    StorageFile.findAll({
      where: { user_id: id },
      order: [["createdAt", "DESC"]],
    })
      .then((data) => {
        if (data.length !== 0) {
          res.status(200).send({
            status: "success",
            data,
          });
        } else {
          res.status(200).send({
            status: "success",
            user_id: id,
            message: "you have no images",
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "some error occured while retrieving docs",
        });
      });
  },

  fetchDoc: (req, res) => {
    const id = req.params.id;

    StorageDoc.findByPk(id, {
      include: [
        {
          model: User,
          attributes: {
            exclude: [
              "password",
              "email",
              "birthdate",
              "gender",
              "address",
              "createdAt",
              "updatedAt",
            ],
          },
        },
      ],
    })
      .then((data) => {
        if (!data) {
          return res.status(401).send({
            status: "failed",
            message: " Storage Doc Not Found",
          });
        }
        res.status(200).send({
          status: "success",
          data,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "error when retriving doc with id " + id,
        });
      });
  },
};
