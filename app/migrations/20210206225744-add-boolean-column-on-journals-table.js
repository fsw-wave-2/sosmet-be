"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn("journals", "set_public", {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        }),
      ]);
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("journals", "set_public"),
      ]);
    });
  },
};
